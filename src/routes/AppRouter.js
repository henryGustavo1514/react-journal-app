import React, { useEffect, useState } from 'react'
import { BrowserRouter as Router, Switch } from 'react-router-dom'
import JournalScreen from '../components/journal/JournalScreen'
import AuthRouter from './AuthRouter'
import { firebase } from '../firebase/firebase-config'
import { useDispatch } from 'react-redux'
import { login } from '../actions/auth'
import PublicRoute from './PublicRoute'
import PrivateRoute from './PrivateRoute'
import { startLoadingNotes } from '../actions/notes'
const AppRouter = () => {
  const dispatch = useDispatch()

  const [checking, setchecking] = useState(true)
  const [isloggedIn, setisloggedIn] = useState(false)

  useEffect(() => {
    firebase.auth().onAuthStateChanged(async user => {
      // console.log(user);
      if (user?.uid) {
        dispatch(login(user.uid, user.displayName))
        setisloggedIn(true)

        dispatch(startLoadingNotes(user.uid))
      } else {
        setisloggedIn(false)
      }
      setchecking(false)
    })
  }, [dispatch, setchecking, setisloggedIn])
  if (checking) {
    return <h1>Wait</h1>
  }
  return (
    
    <Router basename="/journal-app">
      <Switch>
        <PublicRoute
          isAuthenticated={isloggedIn}
          path='/auth'
          component={AuthRouter}
        />
        <PrivateRoute
          isAuthenticated={isloggedIn}
          path='/'
          component={JournalScreen}
        />
       
      </Switch>
    </Router>
  )
}

export default AppRouter
