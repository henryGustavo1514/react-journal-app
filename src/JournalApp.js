import React from 'react'
import AppRouter from './routes/AppRouter'
import { Provider } from 'react-redux'
import { store } from './store/store'
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

const options = {
  // you can also just use 'bottom center'
  position: positions.TOP_RIGHT,
  timeout: 3000,
  offset: '30px',
  // you can also just use 'scale'
  // transition: transitions.SCALE
  transition: transitions.FADE,
}
 
const HournalApp = () => {
  return (
    <AlertProvider template={AlertTemplate} {...options}>
    
    <Provider store={store}>
      <AppRouter />
    </Provider>
  </AlertProvider>
  )
}

export default HournalApp
