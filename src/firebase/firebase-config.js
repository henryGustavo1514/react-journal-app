import firebase from 'firebase'
import 'firebase/firestore'
import 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyCzlVYUJisNR_RVBv-5K_Ii1ZIQQpUyfsw',
  authDomain: 'journal-app-350af.firebaseapp.com',
  projectId: 'journal-app-350af',
  storageBucket: 'journal-app-350af.appspot.com',
  messagingSenderId: '767370931698',
  appId: '1:767370931698:web:7ca409ac8743cb959e0f6b',
  measurementId: 'G-1QHY84MWVX'
}

const firebaseConfigTesting = {
  apiKey: 'AIzaSyBfKAmFb9xaQPt9G-rIA_L7l_bJt0Bu38c',
  authDomain: 'testfirebase001-77a10.firebaseapp.com',
  databaseURL: 'https://testfirebase001-77a10.firebaseio.com',
  projectId: 'testfirebase001-77a10',
  storageBucket: 'testfirebase001-77a10.appspot.com',
  messagingSenderId: '406817408380',
  appId: '1:406817408380:web:03d970b5d97359b85ed0f9'
}
if (process.env.NODE_ENV === 'test') {
  // testing

  firebase.initializeApp(firebaseConfigTesting)
} else {
  //develop
  firebase.initializeApp(firebaseConfig)
}

// firebase.analytics()

const db = firebase.firestore()
const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

export { db, googleAuthProvider, firebase }
