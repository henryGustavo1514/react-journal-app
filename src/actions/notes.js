import { db } from '../firebase/firebase-config'
import { fileUpload } from '../helpers/fileUpload'
import { loadNotes } from '../helpers/loadNotes'
import { types } from '../types/types'

export const startNewNote = () => {
  return async (dispatch, getState) => {
    const { uid } = getState().auth
    const newNote = {
      title: '',
      body: '',
      date: new Date().getTime()
    }
    const doc = await db.collection(`${uid}/journal/notes`).add(newNote)
    dispatch(newAddNote(doc.id, newNote))
  }
}

export const newAddNote = (id, note) => {
 
  return {
    type: types.notesAddNew,
    payload: {
      id,
      ...note
    }
  }
}
export const activeNote = (id, note) => {

  return {
    type: types.notesActive,
    payload: {
      id,
      ...note
    }
  }
}

export const startLoadingNotes = uid => {
  return async dispatch => {
    const notes = await loadNotes(uid)
    dispatch(setNotes(notes))
  }
}

export const setNotes = notes => ({
  type: types.notesLoad,
  payload: notes
})

export const startSaveNotes = (note, alert) => {
  return async (dispatch, getState) => {
    const { uid } = getState().auth
    if (!note.url) {
      delete note.url
    }
    const noteToFirestore = { ...note }
    delete noteToFirestore.id
    await db.doc(`${uid}/journal/notes/${note.id}`).update(noteToFirestore)
    dispatch(refresNote(note.id, noteToFirestore))
    if (alert) {
      alert.success('Nota Actualizada')
    }
  }
}

export const refresNote = (id, note) => ({
  type: types.notesUpadted,
  payload: {
    id,
    note: {
      id,
      ...note
    }
  }
})

export const startUploading = (file, alert) => {
  return async (dispatch, getState) => {
    const { active } = getState().notes
  
    try {
      const fileUrl = await fileUpload(file)
      console.log(fileUrl)
      active.url = fileUrl
      dispatch(startSaveNotes(active, alert))
    } catch (error) {
      console.log(error)
    }
  }
}

export const startDelete = id => {
  return async (dispatch, getState) => {
    const uid = getState().auth.uid
    await db.doc(`${uid}/journal/notes/${id}`).delete()
    dispatch(deleteNote(id))
  }
}

export const deleteNote = id => ({
  type: types.notesDelete,
  payload: id
})

export const notesLogout = () => ({
  type: types.notesLogoutCleaning
})
