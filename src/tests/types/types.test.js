const { types } = require(`../../types/types`)
describe('Pruebas en types.js', () => {
  test('debe ser igual que el objeto evaluado (typesEval) ', () => {
    const typesEval = {
      login: '[Auth] Login',
      logout: '[Auth] Logout',

      uiSetError: '[UI] Set Error',
      uiRemoveError: '[UI] Remove Error',

      uiStartLoading: '[UI] Start loading',
      uiFinishLoading: '[UI] Finish loading',

      notesAddNew: '[Notes] New Note',
      notesActive: '[Notes] Set active Note',
      notesLoad: '[Notes] Load Notes',
      notesUpadted: '[Notes] Update Note',
      notesFileUrl: '[Notes] Uptated image url',
      notesDelete: '[Notes] Delete note',
      notesLogoutCleaning: '[Notes] Logout Cleaning',

      loadSpinn: '[Until] Load Spinner',
      stopSpinn: '[Until] Stop Spinner'
    }
    // console.log(types)
    expect(typesEval).toEqual(types)
  })
})
