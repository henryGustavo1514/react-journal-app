import { authReducer } from '../../reducers/authReducer'
import { types } from '../../types/types'
describe('Pruebas sobre auth Reducer', () => {
  test('debe restornar el estado default', () => {
    const initialState = { uid: 'ew32do2dn2i2', name: 'henry' }
    const state = authReducer(initialState, {})
    // console.log(state)
    expect(state).toEqual(initialState)
  })
  
  test('debe retornar {} en logout', () => {
    const initialState = {}
    const state = authReducer(initialState, types.logout)
    // console.log(state)
    expect(state).toEqual({})
  })

  test('debe realizar el login', () => {
    const initialState = {}
    const action = {
      type: types.login,
      payload: {
        uid: 'abc',
        displayName: 'Henry'
      }
    }

    const state = authReducer(initialState, action)
    // console.log({state})
    expect(state).toEqual({ uid: 'abc', name: 'Henry' })
  })

  test('debe realizar el logout', () => {
    const initialState = { uid: 'dewcw', nombre: 'Henry' }
    const action = {
      type: types.logout
    }

    const state = authReducer(initialState, action)
    expect(state).toEqual({})
  })

  test('debe retornar state por defecto', () => {
    const initialState = { uid: 'dewcw', nombre: 'Henry' }
    const action = {
      type: types.default
    }

    const state = authReducer(initialState, action)
    expect(state).toEqual(initialState)
  })
})
