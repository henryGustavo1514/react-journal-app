import configureStore from 'redux-mock-store' //ES6 modules
import thunk from 'redux-thunk'
import { startLoadingNotes, startUploading } from '../../actions/notes'
import { fileUpload } from '../..//helpers/fileUpload'
jest.mock('../../helpers/fileUpload', () => ({
  fileUpload: jest.fn(() => {
    console.log('objectFileup')
    return 'https://hello-image.com/image.jpg'
  })
}))
const middlewares = [thunk]
const mockStore = configureStore(middlewares)
const initState = {
  auth: {
    uid: 'testing'
  },
  notes: {
    active: {
      id: 'A4YMrhhmrjM3YvH1q1HK',
      title: 'titulo',
      body: 'body text!!'
    }
  }
}
let store = mockStore(initState)
describe('Pruebas con las acciones de notes', () => {
  test('startUploading debe actualizar el URL del entry', async () => {
    const file = new File([], 'image.jpg')
    await store.dispatch(startUploading(file, undefined))
  })
})
