import cloudinary from 'cloudinary'
import { fileUpload } from '../../helpers/fileUpload'

cloudinary.config({
  cloud_name: 'uce',
  api_key: '547826183111797',
  api_secret: 's7m7uqGPnu3BH8YkDHYhP_QMHfg'
})
describe('Pruebas en fileUpload', () => {
  test('Debe cargar un archivo y retornar el URL', async () => {
    const resp = await fetch(`https://picsum.photos/200/300`)

    const blob = await resp.blob()
    // console.log(blob)
    const file = new File([blob], 'foto.png')
    // console.log(file)
    const url = await fileUpload(file)
    // console.log(url)
    expect(typeof url).toBe('string')
    // Borrar imagen por id
    const segments = url.split('/')
    const imageId = segments[segments.length - 1].replace('.jpg', '')
    // console.log(imageId)
    cloudinary.v2.api.delete_resources(imageId, {}, () => {})
  })

  test('Debe cargar un archivo y retornar el URL', async () => {
    const file = new File([], 'foto.png')

    const url = await fileUpload(file)
    expect(url).toBe(null)
  })
})
