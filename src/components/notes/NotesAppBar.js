import React from "react";
import { useDispatch } from "react-redux";
import { startUploading } from "../../actions/notes";
import { useAlert } from "react-alert";
import { loadSpinn, stopSpinn } from "../../actions/spinn";

const NotesAppBar = ({ fecha }) => {
  const alert = useAlert();
  const dispatch = useDispatch();

  const handleFileChange = (e) => {
    dispatch(loadSpinn());
    const file = e.target.files[0];
    if (file) {
      dispatch(startUploading(file, alert));
    }

    setTimeout(() => {
      dispatch(stopSpinn());
    }, 2000);
  };

  return (
    <div className="notes__appbar">
      <input
        id="fileSelector"
        type="file"
        style={{ display: "none" }}
        onChange={handleFileChange}
      />
      <span>{fecha.format("dddd/MM/YYYY")}</span>
    </div>
  );
};

export default NotesAppBar;
