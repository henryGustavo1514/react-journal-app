import React from "react";
import moment from "moment";
import { activeNote } from "../../actions/notes";
import { useDispatch } from "react-redux";
const JournalEntry = ({ id, date, title, body, url }) => {
  const dispatch = useDispatch();
  const noteDate = moment(date);

  const handleActiveEntry = () => {
    dispatch(
      activeNote(id, {
        id: id,
        date: date,
        title: title,
        body,
        url: url,
      })
    );
  };
  return (
    <div className="journal__entry animate__animated animate__fadeInLeft" onClick={handleActiveEntry}>
      {url && (
        <img src={url} alt="" className="journal__image" />
        
      )}

      <div className="info-note">
        <p className="journal__entry-title">{title}</p>
        <p className="journal__entry-content">{body}</p>
      </div>
      <div className="journal__entry-date-box">
        <span>{noteDate.format("dddd")}</span>
        <h4>{noteDate.format("Do")}</h4>
      </div>
     
    </div>
  );
};

export default JournalEntry;
