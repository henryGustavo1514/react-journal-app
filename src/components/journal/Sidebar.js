import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startLogout } from '../../actions/auth'
import { startNewNote } from '../../actions/notes'
import JournalEntries from './JournalEntries'

const Sidebar = () => {
  const { name } = useSelector(state => state.auth)

  const dispatch = useDispatch()
  const handleLogOut = () => {
    dispatch(startLogout())
  }

  const handleAddNew = () => {
    dispatch(startNewNote())
  }
  return (
    <aside className='journal__sidebar'>
      <div className='journal__sidebar-navbar'>
        <h3 className=''>
        <i className="fas fa-user"></i>
          <span> {name}</span>
        </h3>

        <button className='btn btn-secondary' onClick={handleLogOut}>
          Cerrar Sesion
        </button>
      </div>

      <div className='journal__nex-entry' onClick={handleAddNew}>
      <i className="fas fa-plus-circle"></i>
        <p className=''>Nueva Nota</p>
      </div>

      <JournalEntries />
    </aside>
  )
}

export default Sidebar
