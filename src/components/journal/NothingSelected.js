import React from "react";
import Footer from "../../components/auth/Footer";
const NothingSelected = () => {
  return (
    <div className="nothing__main-content">
      <div className="mensaje">
        <p>
          Nada Seleccionado
          <br />
          Cree una nota
        </p>
        <i className="far fa-star fa-4x mt-5"></i>
      </div>

      <Footer />
    </div>
  );
};

export default NothingSelected;
