import React from "react";

const Footer = () => {
  return (
    <div className="footer text-center">
      <div className="info">
        <p>Diseño y Construccion por Henry Tipantuña</p>
        <p>© Henry Tipantuña 2021</p>
      </div>
      <a href="https://henrytipantuna.website" target="_blank" rel="noreferrer">
        Visita mi sitio
      </a>
    </div>
  );
};

export default Footer;
